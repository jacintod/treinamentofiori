sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/m/MessageBox",
	"com/numenit/TreinamentoFiori/utils/object",
	"com/numenit/TreinamentoFiori/singleton/Person",
	"com/numenit/TreinamentoFiori/singleton/Skill",
	"com/numenit/TreinamentoFiori/singleton/services",
	"../keyboard/Keyboard"
], function (Controller, Filter, FilterOperator, MessageBox, objectJacinto, Person, Skill, services, Keyboard) {
	"use strict";

	return Controller.extend("com.numenit.TreinamentoFiori.controller.Main", {

		showTeclado: function (oEvt) {
			Keyboard.getInstance().setModel("modelViewKey", "/input");
			Keyboard.getInstance().setView(this.getView());
			Keyboard.getInstance().montaDefault();
			Keyboard.getInstance().openPopoverBy(this.getView().byId("inputTeste"));

		},
		showTeclado2: function (oEvt) {
			Keyboard.getInstance().setModel("modelViewKey", "/input2");
			Keyboard.getInstance().setView(this.getView());
			Keyboard.getInstance().montaDefaultNumerico();       
			Keyboard.getInstance().openPopoverBy(this.getView().byId("inputTeste2"));

		},

		onInit: function () {
			this.getOwnerComponent().getRouter().getTarget("Main").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
		},

		handleRouteMatched: function (oEvent) {
			this.onSearch();
		},

		onSearch: function (oEvent) {
			// filtro para a busca
			var aFilters = [];

			// assim voce pega pelo event 
			//var sQuery = oEvent.getSource().getValue();

			// assim voce pega pelo id do searchField
			var sQuery = this.getView().byId("searchField").getValue();

			if (sQuery && sQuery.length > 0) {

				aFilters.push(new Filter({
					filters: [
						// adicionando um filtro para o atributo "SALESORDERID" com o operador contains para string, poderia ser EQ ou até mesmo BT
						// a propriedade and com o valor false é para fazer o OR entre SALESORDERID e Buyer/COMPANYNAME
						new Filter({
							path: "SALESORDERID",
							operator: FilterOperator.Contains,
							value1: sQuery,
							and: false
						}),
						new Filter({
							path: "Buyer/COMPANYNAME",
							operator: FilterOperator.Contains,
							value1: sQuery,
							and: false
						})
					]
				}));
			}
			// atualiza o binding "items" da lista, assim ele vai aplicar o filtro que você precisa na busca
			this.byId("list").getBinding("items").filter(aFilters, "Application");
		},

		onSave: function () {
			this._oDialog1.getModel().submitChanges({
				success: function (oData, response) {

				}.bind(this),
				error: function (error) {

				}
			});
		},

		itemPress: function (oEvent) {
			if (!this._oDialog1) {
				this._oDialog1 = sap.ui.xmlfragment("com.numenit.TreinamentoFiori.view.dialogBinding", this);
				this.getView().addDependent(this._oDialog1);
			}
			this._oDialog1.bindObject({
				path: oEvent.getSource().getBindingContextPath()
			});
			this._oDialog1.open();
		},

		oDialogClose: function (oEvent) {
			oEvent.getSource().getParent().close();
		},

		openDialogModel: function () {
			var oApplicationModel = this.getView().getModel("application");

			oApplicationModel.setProperty("/nome", "Danilo");
			oApplicationModel.setProperty("/lassname", "Jacinto");
			oApplicationModel.setProperty("/text", "É uma pessoal legal, contem com ele :)");
			oApplicationModel.refresh();
			if (!this._oDialogModel) {
				this._oDialogModel = sap.ui.xmlfragment("com.numenit.TreinamentoFiori.view.dialogModel", this);
				this.getView().addDependent(this._oDialogModel);
			}
			this._oDialogModel.open();
		},

		openDialogClasee: function (oEvt) {
			var person1 = new Person({
				firstname: "Danilo",
				lastname: "Jacinto"
			});
			person1.addSkill(new Skill({
				name: "Desenvolvedor",
				value: "III"
			}));
			person1.addSkill(new Skill({
				name: "Fiori",
				value: "Mediocre"
			}));
			person1.addSkill(new Skill({
				name: "ABAP",
				value: "Merda"
			}));
			services.getInstance().addPerson(person1);

			var person2 = new Person({
				firstname: "Leon",
				lastname: "Berni"
			});
			person2.addSkill(new Skill({
				name: "Desenvolvedor",
				value: "I"
			}));
			person2.addSkill(new Skill({
				name: "Fiori",
				value: "Top"
			}));
			services.getInstance().addPerson(person2);

			this.getView().setModel(services.getInstance().getModel(), "classe");
			if (!this._oDialogClass) {
				this._oDialogClasse = sap.ui.xmlfragment("com.numenit.TreinamentoFiori.view.dialogClasse", this);
				this.getView().addDependent(this._oDialogClasse);
			}
			this._oDialogClasse.open();

		},

		vaiComTodos: function () {
			var oView = this.getView(),
				oModel = oView.getModel();

			oView.byId("list").getSelectedContextPaths().forEach(function (sPath) {
				services.getInstance().addProduct(oModel.getProperty(sPath + "/SALESORDERID"));
			});

			if (services.getInstance().products.length > 0) {
				this.getOwnerComponent().getRouter().navTo("Second");
			} else {
				MessageBox.show("Selecionar ao menos 1 item");
			}
		},

		pressItem: function (oEvent) {
			var path = this.getView().getModel().getData(oEvent.getSource().getBindingContextPath()).SALESORDERID;

			// passando como path de navegação o numero da sales order
			this.getOwnerComponent().getRouter().navTo("ExemploViewPath", {
				invoicePath: path
			}, true);
		}

	});

});