sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"com/numenit/TreinamentoFiori/utils/object",
	"com/numenit/TreinamentoFiori/singleton/services",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (Controller, objectJacinto, services, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("com.numenit.TreinamentoFiori.controller.Second", {

		onInit: function () {
			this.getOwnerComponent().getRouter().getTarget("Second").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
		},

		handleRouteMatched: function (oEvent) {
			// var produtos = objectJacinto.getSelectedProducts();
			var produtos = services.getInstance().products,
				aFilters = [];
			if (Array.isArray(produtos) && produtos.length > 0) {
				aFilters = produtos.map(function (produto) {
					return new Filter({
						path: "SALESORDERID",
						operator: FilterOperator.EQ,
						value1: produto
					});
				});
			}
			this.byId("listProduct").getBinding("items").filter(aFilters, "Application");
		},

		navBack: function (oEvt) {
			this.getOwnerComponent().getRouter().navTo("Main");
		}

	});

});