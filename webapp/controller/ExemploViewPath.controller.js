sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
], function (Controller, MessageBox) {
	"use strict";

	return Controller.extend("com.numenit.TreinamentoFiori.controller.ExemploViewPath", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf com.numenit.TreinamentoFiori.view.ExemploViewPath
		 */
		onInit: function () {
			this.getOwnerComponent().getRouter().getTarget("ExemploViewPath").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
		},

		handleRouteMatched: function (oEvent) {
			var oView = this.getView(),
				oModel = oView.getModel(),
				oApplicationModel = oView.getModel("application");

			oApplicationModel.setProperty("/icon", "sap-icon://edit");
			oApplicationModel.setProperty("/btnEditar", "Editar");
			oApplicationModel.setProperty("/editavel", false);
			oApplicationModel.setProperty("/create", false);
			oApplicationModel.refresh();

			if (typeof oEvent.getParameters().data.invoicePath !== "undefined") {
				// guardamos esse path caso precisamos usar ele novamente
				this.invoicePath = oEvent.getParameters().data.invoicePath;
				oView.bindObject({
					path: oModel.createKey("/SalesOrderHeader", {
						SALESORDERID: this.invoicePath
					}),
					parameters: {
						expand: "Buyer"
					}
				});
			}
		},

		changeInputs: function (oEvt) {
			var oApplicationModel = this.getView().getModel("application");

			if (Object.keys(this.getView().getModel().getPendingChanges()).length !== 0) {
				oApplicationModel.setProperty("/icon", "sap-icon://save");
				oApplicationModel.setProperty("/btnEditar", "Salvar");
				oApplicationModel.refresh();
			}
		},

		setEdit: function () {
			var oView = this.getView(),
				oModel = oView.getModel(),
				oApplicationModel = oView.getModel("application");

			if (Object.keys(oModel.getPendingChanges()).length === 0) {
				var bEditavel = oApplicationModel.getProperty("/editavel");

				oApplicationModel.setProperty("/icon", bEditavel ? "sap-icon://edit" : "sap-icon://display");
				oApplicationModel.setProperty("/btnEditar", bEditavel ? "Editar" : "Exibir");
				oApplicationModel.setProperty("/editavel", !bEditavel);

			} else {
				oModel.submitChanges({
					success: function (oData, response) {
						// MessageBox.show("Editado");
					}.bind(this),
					error: function (error) {

					}
				});
				oApplicationModel.setProperty("/icon", "sap-icon://edit");
				oApplicationModel.setProperty("/editavel", false);
			}
			oApplicationModel.refresh();
		},

		navBack: function (oEvt) {
			// esse reset changes é pra se o cara voltar sem dar salvar ele limpar as ediçoes que ele fez
			this.getView().getModel().resetChanges();
			this.getOwnerComponent().getRouter().navTo("Main");
		},

		createSalesOrder: function (oEvt) {
			var oView = this.getView(),
				oModel = oView.getModel(),
				oApplicationModel = oView.getModel("application");

			if (!oApplicationModel.getProperty("/create")) {
				var min = 500000500,
					max = 590000000;
				this.random = Math.round(Math.random() * (+max - +min) + +min);

				var oEntry = oModel.createEntry("/SalesOrderHeader", {
					properties: {
						"SALESORDERID": this.random.toString(),
						"HISTORY.CREATEDBY.EMPLOYEEID": "0000000033",
						"HISTORY.CREATEDAT": "/Date(1388534400000)/",
						"HISTORY.CHANGEDBY.EMPLOYEEID": "0000000033",
						"HISTORY.CHANGEDAT": "/Date(1389744000000)/",
						"NOTEID": null,
						"PARTNER.PARTNERID": "0100000000",
						"CURRENCY": "EUR",
						"GROSSAMOUNT": "26581.03",
						"NETAMOUNT": "22337",
						"TAXAMOUNT": "4244.03",
						"LIFECYCLESTATUS": "X",
						"BILLINGSTATUS": "I",
						"DELIVERYSTATUS": "I"
					}
				});

				oView.bindObject({
					path: oEntry.getPath()
				});
				oApplicationModel.setProperty("/create", true);
				oApplicationModel.refresh();
			} else {
				oModel.submitChanges({
					success: function (oData, response) {
						// lembra de mapear essa respota que irá vir no oData, pois pode haver mais de uma change, voce precisa saber qual é o seu
						MessageBox.show("Criado");
						// aqui estou setando na view o path do criado para ele buscar o nosso criado no banco
						oView.bindObject({
							path: oModel.createKey("/SalesOrderHeader", {
								SALESORDERID: this.random.toString()
							}),
							parameters: {
								expand: "Buyer"
							}
						});
					}.bind(this),
					error: function (error) {

					}
				});

				oApplicationModel.setProperty("/create", false);
				oApplicationModel.refresh();

			}
		},

		deleteSalesOrder: function (oEvt) {
			var oModel = this.getView().getModel();

			oModel.remove(oModel.createKey("/SalesOrderHeader", {
				SALESORDERID: this.invoicePath
			}), {
				success: function (oData, response) {
					// como voce deletou não tem por que estar nessa tela ainda
					this.navBack();
					MessageBox.show("Deletado");
				}.bind(this),
				error: function (error) {

				}
			});
		}

	});

});