sap.ui.define([
	"com/numenit/TreinamentoFiori/singleton/BaseObject",
	"sap/ui/model/json/JSONModel",
	"./keyboardButton"
], function (Object, JSONModel, keyboardButton) {
	"use strict";

	return Object.extend("com.numenit.TreinamentoFiori.keyboard.keyboardLinha", {

		constructor: function (arrayButtons, properties) {
			// Object.call(this);
			this.keyboardButton = [];
			if (arrayButtons) {
				this.keyboardButton = arrayButtons;
			}
			this.properties = {};
			if (properties) {
				this.properties = properties;
			}
			this.keyboardHbox = this.createHbox();
			this.addButtonsToContent();
		},
		getHbox: function(){
			return this.keyboardHbox;
		},
		addButtonsToContent: function(){
			for(var i = 0; i < this.keyboardButton.length; i++ ){
				this.keyboardHbox.addItem(this.keyboardButton[i].getButton());
			}	
		},
		createHbox: function () {
			return new sap.m.HBox(this.properties);
		}

	});
});