sap.ui.define([
	"com/numenit/TreinamentoFiori/singleton/BaseObject",
	"sap/ui/model/json/JSONModel",
	"./keyboardButton",
	"./keyboardLinha",
	"./modelDefault"
], function (Object, JSONModel, keyboardButton, keyboardLinha, modelDefaultJS) {
	"use strict";
	var instance;

	var Keyboard = Object.extend("com.numenit.TreinamentoFiori.keyboard.Keyboard", {

		setModel: function (modelName, modelPath, modelLength) {
			this.modelName = modelName;
			this.modelPath = modelPath;
			this.modelLength = modelLength;
		},
		btnClick: function (oEvt) {
			var tecla = oEvt.getSource().getText();
			if (tecla === ",") {
				tecla = ".";
			}

			var texto = "";
			var model = null;
			if (this.modelName !== "") {
				model = this._oView.getModel(this.modelName);
			} else {
				model = this._oView.getModel();
			}
			if (model === null) {
				return;
			}
			texto = model.getProperty(this.modelPath);
			if (tecla === "." && texto.indexOf(".") > -1) {
				return;
			}
			if (this.zeroInsert === true) {
				this.zeroInsert = false;
				texto = texto.slice(0, texto.length - 1);
			}
			switch (tecla) {
			case "Space":
				texto = texto + " ";
				break;
				// BackSpace
			case "":
				if (oEvt.getSource().getIcon() === "sap-icon://undo") {
					texto = texto.slice(0, texto.length - 1);
				}
				break;
			default:
				texto = texto + tecla;
				break;
			}

			var last = texto.slice(texto.length - 1, texto.length);
			if (last === ".") {
				texto = texto + "0";
				this.zeroInsert = true;
			}

			if (this.modelLength !== null) {
				if (this.modelLength !== undefined) {
					var modelLengthInt = 0;
					try {
						modelLengthInt = parseInt(this.modelLength);
					} catch (error) {
						modelLengthInt = texto.length;
					}
					if (texto.length > modelLengthInt) {
						return;
					}
				}
			}
			model.setProperty(this.modelPath, texto);
		},
		montaDefault: function () {
			this.hboxes = [];
			var linhas = modelDefaultJS.getDefaultTeclado();
			this.criaHboxes(linhas, "A");
			this.addHboxToPopover();
		},
		montaDefaultNumerico: function () {
			this.hboxes = [];
			var linhas = modelDefaultJS.getDefaultTecladoNumerico();
			this.criaHboxes(linhas, "N");
			this.addHboxToPopover();
		},
		montaDefaultNumSemVirgula: function () {
			this.hboxes = [];
			var linhas = modelDefaultJS.getDefaultTecladoNumSemVirgula();
			this.criaHboxes(linhas, "N");
			this.addHboxToPopover();
		},
		addHboxToPopover: function () {
			this._popover.removeAllContent();
			for (var i = 0; i < this.hboxes.length; i++) {
				this._popover.addContent(this.hboxes[i].getHbox());
			}
		},
		criaHboxes: function (linhas, tipo) {
			this.hboxes = [];
			var heightHbox = "";
			var spacePopover = "";
			if (tipo === "N") {
				heightHbox = "4.4em";
				spacePopover = "2em";
			} else {
				heightHbox = "auto";
				spacePopover = "1em";
			}
			for (var i = 0; i < linhas.length; i++) {
				var btnsLinha = [];
				for (var o = 0; o < linhas[i].length; o++) {
					btnsLinha.push(new keyboardButton(linhas[i][o].text, linhas[i][o].width, this.btnClick.bind(this), linhas[i][o].classCss,
						linhas[
							i][o].iconBtn));
				}
				this.hboxes.push(new keyboardLinha(btnsLinha, {
					"alignItems": "Center",
					"alignContent": "Center",
					"justifyContent": "Center",
					"height": heightHbox
				}));
			}

			var btnsLinha = [];
			this.hboxes.push(new keyboardLinha(btnsLinha, {
				"alignItems": "Center",
				"alignContent": "Center",
				"justifyContent": "Center",
				"height": spacePopover
			}));
			// this.hboxes[this.hboxes.length -1 ].keyboardHbox.addStyleClass = "marginBotton";   
		},
		openPopoverBy: function (component) {
				this._popover.openBy(component);
			}
	});
	return {

		getInstance: function () {
			if (!instance) {
				instance = new Keyboard(null, "", "");
			}
			return instance;
		}

	};
});