sap.ui.define([
	"sap/ui/base/Object"
], function (Object) {
	"use strict";
	return {

		getDefaultTeclado: function () {
			var linhas = [];
			linhas.push([{
				text: "1"
			}, {
				text: "2"
			}, {
				text: "3"
			}, {
				text: "4"
			}, {
				text: "5"
			}, {
				text: "6"
			}, {
				text: "7"
			}, {
				text: "8"
			}, {
				text: "9"
			}, {
				text: "0"
			}, {
				// BackSpace
				text: "",
				iconBtn: "sap-icon://undo"
			}]);
			linhas.push([{
				text: "Q"
			}, {
				text: "W"
			}, {
				text: "E"
			}, {
				text: "R"
			}, {
				text: "T"
			}, {
				text: "Y"
			}, {
				text: "U"
			}, {
				text: "I"
			}, {
				text: "O"
			}, {
				text: "P"
			}]);
			linhas.push([{
				text: "A"
			}, {
				text: "S"
			}, {
				text: "D"
			}, {
				text: "F"
			}, {
				text: "G"
			}, {
				text: "H"
			}, {
				text: "J"
			}, {
				text: "K"
			}, {
				text: "L"
			}, {
				text: "Ç"
			}]);
			linhas.push([{
				text: "Z"
			}, {
				text: "X"
			}, {
				text: "C"
			}, {
				text: "V"
			}, {
				text: "B"
			}, {
				text: "N"
			}, {
				text: "M"
			}, {
				text: ","
			}, {
				text: "."
			}]);
			linhas.push([{
				text: "Space",
				width: "20em"
			}]);

			return linhas;
		},
		getDefaultTecladoNumerico: function () {
			var linhas = [];
			linhas.push([{
				text: "7",
				classCss: "marginTeste"
			}, {
				text: "8",
				classCss: "marginTeste"
			}, {
				text: "9",
				classCss: "marginTeste"
			}]);
			linhas.push([{
				text: "4",
				classCss: "marginTeste"
			}, {
				text: "5",
				classCss: "marginTeste"
			}, {
				text: "6",
				classCss: "marginTeste"
			}]);
			linhas.push([{
				text: "1",
				classCss: "marginTeste"
			}, {
				text: "2",
				classCss: "marginTeste"
			}, {
				text: "3",
				classCss: "marginTeste"
			}]);
			linhas.push([{
				text: "0",
				classCss: "marginTeste"
			}, {
				text: ",",
				classCss: "marginTeste"
			}, {
				// BackSpace
				text: "",
				iconBtn: "sap-icon://undo",
				classCss: "marginTeste"
			}]);
			return linhas;
		},
		getDefaultTecladoNumSemVirgula: function () {
			var linhas = [];
			linhas.push([{
				text: "7",
				classCss: "vtBtnNumerico"
			}, {
				text: "8",
				classCss: "vtBtnNumerico"
			}, {
				text: "9",
				classCss: "vtBtnNumerico"
			}]);
			linhas.push([{
				text: "4",
				classCss: "vtBtnNumerico"
			}, {
				text: "5",
				classCss: "vtBtnNumerico"
			}, {
				text: "6",
				classCss: "vtBtnNumerico"
			}]);
			linhas.push([{
				text: "1",
				classCss: "vtBtnNumerico"
			}, {
				text: "2",
				classCss: "vtBtnNumerico"
			}, {
				text: "3",
				classCss: "vtBtnNumerico"
			}]);
			linhas.push([{
				text: "0",
				classCss: "vtBtnNumerico"
			}, {
				// BackSpace
				text: "",
				iconBtn: "sap-icon://undo",
				classCss: "vtBtnNumerico"
			}]);
			return linhas;
		}

	};
});