sap.ui.define([
	"com/numenit/TreinamentoFiori/singleton/BaseObject",
	"sap/ui/model/json/JSONModel"
], function (Object, JSONModel) {
	"use strict";

	return Object.extend("com.numenit.TreinamentoFiori.keyboard.keyboardButton", {

		constructor: function (text, width, press, classCss, iconBtn) {
			// Object.call(this);
			this.text = text;
			this.width = "auto";
			if (width !== null && width !== undefined) {
				this.width = width;
			}
			
			this.press = press;
			this.button = this.createButton();
			
			if (classCss !== null && classCss !== undefined) {
				this.button.addStyleClass(classCss);
			}
			if (iconBtn !== null && iconBtn !== undefined) {
				this.button.setIcon(iconBtn);
			}
		},
		getButton: function(){
			return this.button;
		},
		createButton: function () {
			return new sap.m.Button({
				text: this.text,
				width: this.width,
				press: this.press
			});
		}

	});
});