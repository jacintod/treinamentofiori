sap.ui.define([
	"com/numenit/TreinamentoFiori/singleton/BaseObject",
	"com/numenit/TreinamentoFiori/singleton/Person",
	"com/numenit/TreinamentoFiori/singleton/Skill"
], function (Object, Person, Skill) {
	"use strict";

	var instance;

	var Services = Object.extend("com.numenit.TreinamentoFiori.singleton.Services", {

		constructor: function () {
			Object.call(this);
			this.person = [];
			this.products = [];
			// this.skill = null;
		},

		setProducts: function (Array) {
			this.products = Array;
			this.model.refresh();
		},

		addProduct: function (product) {
			this.products.push(product);
			this.model.refresh();
		},

		addPerson: function (person) {
				this.person.push(person);
				this.model.refresh();
			}
			// setPerson: function (firstname, lastname) {
			// 	this.person.add(new Person({
			// 		firstname: firstname,
			// 		lastname: lastname
			// 	}));
			// },
			// newSkill: function (skill) {
			// 	this.person.addSkill(skill);
			// },
			// getInitialSkill: function () {
			// 	this.skill = new Skill({
			// 		name: "name",
			// 		value: "value"
			// 	});
			// 	return this.skill;
			// }
	});

	return {

		getInstance: function () {
			if (!instance) {
				instance = new Services();
			}
			return instance;
		}

	};

});