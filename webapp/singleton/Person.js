sap.ui.define([
	"com/numenit/TreinamentoFiori/singleton/BaseObject",
	"sap/ui/model/json/JSONModel"
], function (Object, JSONModel) {
	"use strict";

	return Object.extend("com.numenit.TreinamentoFiori.singleton.Person", {

		constructor: function (data) {
			Object.call(this);
			if (data) {
				this.firstname = data.firstname;
				this.lastname = data.lastname;
				this.fullname = this.getFullName();
			}
			this.skills = [];
		},

		getFullName: function () {
			return this.firstname + " " + this.lastname;
		},

		addSkill: function (skill) {
			this.skills.push(skill);
			this.model.refresh();
		}

	});
});