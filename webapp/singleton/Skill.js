sap.ui.define([
	"com/numenit/TreinamentoFiori/singleton/BaseObject"
], function (BaseObject) {
	"use strict";

	return BaseObject.extend("com.numenit.TreinamentoFiori.objects.model.Skill", {

		constructor: function (data) {
			BaseObject.call(this);

			if (data) {
				this.name = data.name;
				this.value = data.value;
			}
		}

	});
});