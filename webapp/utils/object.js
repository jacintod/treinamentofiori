sap.ui.define([
	"sap/ui/base/Object"
], function (Object) {
	"use strict";
	return {

		product: null,

		productsSelected: [],

		getProduto: function () {
			return this.product;
		},

		setProduto: function (produto) {
			this.product = produto;
		},

		setProductSelected: function (ArrayProdutos) {
			this.productsSelected = ArrayProdutos;
		},

		getSelectedProducts: function () {
			return this.productsSelected;
		}

	};
});